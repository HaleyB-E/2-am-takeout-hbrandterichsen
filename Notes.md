This file is for taking notes as you go.


Repo foked approximately 6:24. Ready to start take-home at 6:49. I have never used gitlab, go, or vue before, and I haven't done much/any work with websockets (all my experience is with RESTful API + javascript preloading from C# code), so this should be an adventure.

Started timer 6:50

Question 1: There is a test for the backend that is failing, fix the app code so it passes
- immediately discovered that TestHealthCheck is failing because it's checking for status 'ok' instead of 'OK'. 
- Initially wanted to change test to look for "OK" instead, then reread question and saw it asked for fixing the app code. 
- Changed handlers.go to use "ok", then encountered "struct field ok has json tag but is not exported" error. 
- Upon googling, found https://go.dev/ref/spec#Exported_identifiers which informed me I need to capitalize in order to export. 

- Am now going with initial thought and modifying test to match output, since output is already in spec

Question 2: There is a test for the backend where the assumptions about what needs to be tested are incorrect. The test passes, but doesn't guarantee compatibility with how the frontend expects to communicate. You need to think about how the two interact and are compatible, and change the test code to match.

- start by running server and app to see what's what. Saw console log "backend check failed, response code was 200, data was [object Object]" - not sure if that's related to the test failure, but certainly seems incorrect, so let's look into that first

-Turns out it's the "ok" situation from before (i.e. frontend code is expecting "ok", and getting "OK". Updated to check for status "OK" - at this point, I am less certain about my solution to question 1 but am going to keep going as-is for the sake of making progress.

- looking back at the tests again. Unmarshal = parse from json into variables in any of a couple different ways. This is a new concept for me, I'm going to treat it like a fancier json.parse until I learn otherwise

- I'm having a few spidey senses about "Access-Control-Allow-Origin" but have been assured by the bonus question that this is not an issue. ignore it.

- it looks like there is some reason you can't use fmt.printf as-is in go. first attempt at debugging statements failed.

Swapping over to problem 3 for a bit, maybe getting a stronger sense of what's going on with the frontend wll help me test assumptions for problem 2.

Problem 3: In the frontend, make it so the user can enter a name that gets sent with every message and displayed to other users.

- first step: add row/col wrapper around message textbox and add username textbox
- then add userName parameter to sendMessage to save values to server
- then update formatting around chips to include username (in form "`userName` says:" floating left, which will not format well if message uuid !== uuid. Revisit when doing cleanup)
- added auto-clear after submit to message text, but not username, as basic quality of life update

Ok, that helped a little with learning the frontend, though it only had minimal interaction with the server. Time to go back to test updating (problem 2).

- it looks like the problem might be in the websocket test. Specifically, it checks that the message "hello, websocket!" goes through, but I'm not sure whether that will help verify that the JSON objects maintain the expected shape. We do stringify before we send to the backend and then parse the data, though, so I don't think that's the issue

- Console logged the result of this.$socket.onmessage to look at its shape. It is a MessageEvent, which seems to be what the test is looking for.

- might as well go back and update the test to stringify, send, and unstringify a correctly-shaped JSON blob for completeness' sake, even if it isn't what's "really" wrong. Doing that now.

- that felt enough like a useful thing to do that I want to believe it was correct, but I'm bothered by the fact that we sent stringified json. That really does imply there's something else that I'm missing, but I'm feeling hampered by the fact that everything here is new enough to me that it feels like all my instincts are slightly off. Similar thoughts on the security stuff - I know what general smells are for good/bad practices, but lack of familiarity with the best practices with go + vue + websockets mean I'm extremely vulnerable to Not Knowing What The Right Assumptions To Check Are. 

Security notes:
- this is all using http, not https. This feels too obvious to be the security issue(?) and also somewhat likely to be something that is intentional since all of this is in dev mode rather than prod-ready code anyway.
- basic xss (<script>alert('xss')</script> in username/message text) reveals no obvious issue, but that's a front-end problem - I don't know enough Go to know what to paste in to check for xss, so I tried a couple variants on "log.Printf("ahahaha")" but didn't see anything come up obviously indicating it was that. Lack of golang familiarity strikes again!
- googling about common websocket security issues suggests that using 'ws' instead of 'wss' is insecure - this falls under the same umbrella of http/https, where I'm getting lost in the meta of "is this a thing you expect me to change in a take-home". So I'm just going to mention it as another thing I noticed in the hopes that that provides you enough insight into how my brain looks at problems - I would start with the instructions here https://web.dev/how-to-use-local-https/ were I to try to swap over to wss/https.

Summary of style changes:
- when adding username, updated text input to a) take up only part of the screen, inline with username input
- allowed username to persist after sending a message, but cleared out "New Message" textbox because it seems like people would want a consistent username across messages and also like people would by default not want to send the same message multiple times

Notes on what I'd do if I had more time:
- The formatting for the username *will not* look good once you start having multiple uuids involved, since it's on the left it will squish the left-floating messages from another uuid rightwards by whatever the width of "username says:" is, which is variable in width and therefore gross. Obvious improvement option is to make it kind of like what real messaging apps do and have it float above the first consecutive message of a given author
- if you wanted to add a bit of Name-Silliness-Proofing you could tie usernames to uuids by making the username read-only if the current uuid matched any existing message's uuid, and populating the username field with the username linked with the current uuid in that manner. This does not stop people from duplicating someone else's name, just stops them from changing their own (until they refresh the page)
- you could further lock this down by adding a uniqueness constraint that rejects any message with a username that exists in the system that does not have a uuid that is associated in the system with that username (so if person A used username PacoIsAWinner, person B could not use that username unless they had person A's uuid)

Ended test at 9:04 (gave myself 15 minutes of fudge time due to nvm install issues - thought I had it already set up on this machine, turns out I did not)